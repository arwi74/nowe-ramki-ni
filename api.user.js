// ==UserScript==
// @name         Yuki API
// @namespace    http://tampermonkey.net/
// @version      0.0.1a
// @description  Simple margonem API
// @author       Waleczny Yuuki / Sebastian Łukaszewski
// @match        http://*.margonem.pl/
// @grant        none
// ==/UserScript==
new (function() {
    if(typeof Engine == undefined) {
        document.yuki = this;
    } else {
        window.API.yuki = this;
    }
    this.addStorageData =  function(name, data) {
        localStorage.setItem(name, JSON.stringify(data));
    }
    this.getStorageData = function(name) {
        var d = localStorage.getItem(name);
        return d;
    }
    this.init = function() {
        return console.log("Yuki API Initialization Completed.");
    }
    this.checkBot = function() {
        var x = {
            mLogin: localStorage.getItem("mLogin"),
            mMaps: localStorage.getItem("mMaps"),
            mMobs: localStorage.getItem("mMobs"),
            mMode: localStorage.getItem("mMode"),
            mRoad: localStorage.getItem("mRoad")
        }
        if(x.mLogin != null) {
            if(typeof Engine == undefined) {
                return console.warn("Pliki bota wykryte! \n\nDowody:\n Login: "+x.mLogin+"\n Mapy: "+x.mMaps+"\n Moby: "+x.mMobs+"\n Status bota: "+x.mMode+"\n Ścieżka Dojścia: "+x.mRoad+"\n\n Nick Gracza: "+hero.nick+"\n ID Gracza: "+hero.id+"\n ID Mapy: "+map.id);
            } else {
                return console.warn("Pliki bota wykryte! \n\nDowody:\n Login: "+x.mLogin+"\n Mapy: "+x.mMaps+"\n Moby: "+x.mMobs+"\n Status bota: "+x.mMode+"\n Ścieżka Dojścia: "+x.mRoad+"\n\n Nick Gracza: "+Engine.hero.d.nick+"\n ID Gracza: "+Engine.hero.d.id+"\n ID Mapy: "+Engine.map.d.id);
            }
        } else {
            return console.log("Brak plików bota.");
        }
    }
})();
