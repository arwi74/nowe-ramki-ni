//////////////////////////////////////////
// Autor: arwi74                        //
// Wszystkie prawa zastrzeżone (c)      //
// execstudiodev@gmail.com              //
//////////////////////////////////////////

function BlogoUI() {
    var __this = this;
    var mainBox = $(`
                    <div id="blogoui">
                    	<button class="SI-button abtn" onclick="useblogo(1);">50k</button>
                    	<button class="SI-button abtn" onclick="useblogo(2);">100k</button>  
                    	<button class="SI-button abtn" onclick="useblogo(3);">200k</button>
                    	<button class="SI-button abtn" onclick="useblogo(4);">400k</button>
                        <button class="SI-button abtn" onclick="useblogo(5);">600k</button>
                        <button class="SI-button abtn" onclick="useblogo(6);">800k</button>
                        <button class="SI-button abtn" onclick="useblogo(7);">1m</button>
                    </div>`).css({
            width: 70,
            zIndex: 391,
            background: '#222',
            border: '3px double #A95',
            color: '#FFD700',
            fontFamily: 'Verdana, Arial, sans-serif',
            fontSize: '11px',
            textAlign: 'center',
            position: 'absolute',
            padding: '3px 3px 4px',
            cursor: 'grab',
            height: 187,
            overflow: 'hidden'
        }).draggable({
            opacity: 0.7,
            start: function () {
                g.lock.add('dragging');
                Tip.disable();
                $(this).css({
                    cursor: 'grabbing'
                });
            },
            stop: function () {
                __this.saveSettings();
                g.lock.remove('dragging');
                Tip.enable();
                $(this).css({
                    cursor: 'grab'
                });
            }
        });
    $('body').append(mainBox);
    var script = `
        <script>
            function useblogo(id) {
                if(id == 1) {
                  _g("clan&a=skills_use&name=blessing&opt=1")
                } else if(id == 2) {
                  _g("clan&a=skills_use&name=blessing&opt=2")
                } else if(id == 3) {
                  _g("clan&a=skills_use&name=blessing&opt=3")
                } else if(id == 4) {
                  _g("clan&a=skills_use&name=blessing&opt=4")
                } else if(id == 5) {
                  _g("clan&a=skills_use&name=blessing&opt=5")
                } else if(id == 6) {
                  _g("clan&a=skills_use&name=blessing&opt=6")
                } else if(id == 7) {
                  _g("clan&a=skills_use&name=blessing&opt=7")
                }
            }
        </script>
    `;
  var style = `
  	<style>
      .abtn {
        height: 25px;
        color: white;
        padding: 0;
        margin-top: 1px;
        margin-bottom: 1px;
        background: rgb(23,23,23);
        font-size: 12px;
        border: 1px solid blue;
      }
  .abtn:hover {
    background: black;
    border: 1px solid lightblue;
  }
      </style>
  `;
    $('body').append(script);
  	$('head').append(style);
    var tmpLootItem = lootItem;
    var doubleSendBlock = false;
    this.saveSettings = function () {
        localStorage.lootfilterU = 'top:' + mainBox.offset().top + '|left:' + mainBox.offset().left;
    };
    this.readSettings = function () {
        var settings = getCookie('blogocookie');
        if (!!localStorage.lootfilterU) {
            var settings = localStorage.lootfilterU;
        }
        if (settings) {
            settings = settings.split('|');
            for (i = 0; i < settings.length; i++) {
                var pair = settings[i].split(':');
                switch (pair[0]) {
                case 'top':
                    mainBox.css('top', pair[1] + 'px');
                    break;
                case 'left':
                    mainBox.css('left', pair[1] + 'px');
                    break;
                }
            }
        }
    };
    this.readSettings();
};
g.loadQueue.push({
    fun: function () {
        var _blg = new BlogoUI();
    },
    data: ''
});
