// ==UserScript==
// @name         Motyw NI
// @namespace    http://tampermonkey.net/
// @version      1.6.1
// @description  Dodatek pozwala zmieniać wyglą rangi przedmiotów
// @author       Waleczny Yuuki
// @match        http://*.margonem.pl/
// @grant        none
// ==/UserScript==
// ==Changelog==
// 1.6.1 - Zmieniono pozycję menu oraz przycisku na czas testów czy będzie się lepiej sprawował niż w poprzedniej wersji.
// 1.6 - Poprawiono błąd z nieładującymi się ramkami podczas dużego pingu
// 1.5 - Dodano przycisk do ukrywania menu
// ==/Changelog==

window.yMotyw = new function() {

    if (typeof Engine == "undefined" || typeof parseClanBB == "undefined" || typeof linkify == "undefined") return;

    this.data = {

    }

    this.setDefaultValuesY = function() {
        localStorage.setItem("yRamki", JSON.stringify(this.dataDefault));
        message("Zmiany będą widoczne po odświeżeniu gry");
    }

    this.saveValuesY = function() {
        var dataFSave = {
            "img": $('#img-yuki')[0].value,
            "legend": $('#legend-yuki')[0].value,
            "heroic": $('#heroic-yuki')[0].value,
            "unique": $('#unique-yuki')[0].value,
            "upgraded": $('#upgraded-yuki')[0].value
        }
        localStorage.setItem("yRamki", JSON.stringify(dataFSave));
        message("Zmiany będą widoczne po odświeżeniu gry");
    }

    this.dataDefault = {
        "img": "https://i.imgur.com/OM178Yk.gif",
        "legend": "#e100c6",
        "heroic": "#0b4ac1",
        "unique": "#008200",
        "upgraded": "#b9e701"
    }

    function inityMotywMenu() {
        $(".inventory_wrapper").append(`
       <div id='yMotywMenu' style='height: 320px;width: 227px; text-align: center;position: absolute;top: 240px;left: -230px;margin-right: 7px;background: rgba(0,0,0,0.7);'>
           <p style='color: white; font-size: 19px; text-align:center; padding-top: 20px; line-height: 0px;'>Panel Motywu</p><br>
           <p style='color: white; font-size: 12px; text-align: center; line-height: 0px;'>by <a href="https://www.margonem.pl/?task=profile&id=8953502" target="_blank" style='color: white;'>Yuuki</a></p><br><br>
       <div style='text-align: left; padding-left: 10px;'>
           <p style='font-size: 16px; color: white;'>Link do ramek: <input type='text' id='img-yuki' class='yuki-input' value='${this.data.img}' style='background: rgba(0, 0, 0, 0.2); border: none; width: 40%; color: white; padding: 5px; resize: none; outline: none;'><br>
           <p style='font-size: 16px; color: white;'>Kolor Legend: <input type='color' value='${this.data.legend}' id='legend-yuki' class='yuki-input-color' style='background: rgba(0, 0, 0, 0.2); border: none; width: 10%; color: white; padding: 5px; resize: none; outline: none;'><br>
           <p style='font-size: 16px; color: white;'>Kolor Heroików: <input type='color' value='${this.data.heroic}' id='heroic-yuki' class='yuki-input-color' style='background: rgba(0, 0, 0, 0.2); border: none; width: 10%; color: white; padding: 5px; resize: none; outline: none;'><br>
           <p style='font-size: 16px; color: white;'>Kolor Unikatów: <input type='color' value='${this.data.unique}' id='unique-yuki' class='yuki-input-color' style='background: rgba(0, 0, 0, 0.2); border: none; width: 10%; color: white; padding: 5px; resize: none; outline: none;'><br>
           <p style='font-size: 16px; color: white;'>Kolor Ulepszonych: <input type='color' value='${this.data.upgraded}' id='upgraded-yuki' class='yuki-input-color' style='background: rgba(0, 0, 0, 0.2); border: none; width: 10%; color: white; padding: 5px; resize: none; outline: none;'><br><br>
           <button onclick='yMotyw.saveValuesY();' class='button small green'>Zapisz</button> <button onclick='yMotyw.setDefaultValuesY();' class='button small'>Domyślne ustawienia</button><br><br>
<button class='button small green' style='font-size: 10px; height: 25px' onclick='yMotyw.hideYukiMenu();'>Ukryj Menu</button>
      </div>
      </div>
`);

    }

    this.hideYukiMenu = function hideYukiMenu() {
        $("#yMotywMenu").css({display: 'none'});
        $(".bottom").append(`
<button id="yMotywButton" class='button small green' style='position: absolute; top: 20px; left: 1500px; font-size: 10px; height: 25px' onclick='yMotyw.showYukiMenu();'>Pokaż Yuki Menu</button>
`);
        localStorage.setItem("yRamkiStatus", false);
    }

    this.showYukiMenu = function showYukiMenu() {
        $("#yMotywMenu").css({display: 'block'});
        $("#yMotywButton").remove();
        localStorage.setItem("yRamkiStatus", true);
    }

    function initGraphics() {
      $(".highlight").css({
        "backgroundImage": "url(" + this.data.img + ")"
      });

      var x = `
.tip-wrapper[data-type=t_item] .item-head .legendary { color: ${this.data.legend} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .legendary { color: ${this.data.legend} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .legendary, .tip-wrapper[data-type=t_item] .item-tip-section .legendary{ color: ${this.data.legend} !IMPORTANT }
       .tip-wrapper[data-item-type=t-leg] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${this.data.legend},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .tip-wrapper[data-type=t_item] .item-head .heroic { color: ${this.data.heroic} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .heroic { color: ${this.data.heroic} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .heroic, .tip-wrapper[data-type=t_item] .item-tip-section .heroic{ color: ${this.data.heroic} !IMPORTANT }
       .tip-wrapper[data-item-type=t-her] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${this.data.heroic},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .tip-wrapper[data-type=t_item] .item-head .unique { color: ${this.data.unique} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .unique { color: ${this.data.unique} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .unique, .tip-wrapper[data-type=t_item] .item-tip-section .unique{ color: ${this.data.unique} !IMPORTANT }
       .tip-wrapper[data-item-type=t-uniupg] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${this.data.unique},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .tip-wrapper[data-type=t_item] .item-head .upgraded { color: ${this.data.upgraded} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .upgraded { color: ${this.data.upgraded} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .upgraded, .tip-wrapper[data-type=t_item] .item-tip-section .upgraded{ color: ${this.data.upgraded} !IMPORTANT }
       .tip-wrapper[data-item-type=t-upgraded] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${this.data.upgraded},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }
`;
      $('head').append('<style>' + x + '</style>');

    }

    function initAddon() {
        if($(".inventory_wrapper").length > 0) {
            if (localStorage.getItem("yRamki") == null) {
                var dane = {
                    "img": "https://i.imgur.com/OM178Yk.gif",
                    "legend": "#e100c6",
                    "heroic": "#0b4ac1",
                    "unique": "#008200",
                    "upgraded": "#b9e701"
                }
                localStorage.setItem("yRamki", JSON.stringify(dane));
                const obj = localStorage.getItem("yRamki");
                this.data = JSON.parse(obj);
            } else {
                const obj = localStorage.getItem("yRamki");
                this.data = JSON.parse(obj);
            }
            if(localStorage.getItem("yRamkiStatus") == null) {
                localStorage.setItem("yRamkiStatus", true);
                inityMotywMenu();
            } else {
                if(localStorage.getItem("yRamkiStatus") == "true") {
                    inityMotywMenu();
                } else {
                    inityMotywMenu();
                    $("#yMotywMenu").css({display: 'none'});
        $(".bottom").append(`
<button id="yMotywButton" class='button small green' style='position: absolute; top: 20px; left: 1500px; font-size: 10px; height: 25px' onclick='yMotyw.showYukiMenu();'>Pokaż Yuki Menu</button>
`);
                }
            }
            initGraphics();
        }
    }

    initAddon();
}
