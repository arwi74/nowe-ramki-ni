# Wprowadzenie
## Podstawowe informacje na temat projektu

- Nazwa Projektu: *Yuki Margonem API*
- Wersja: *0.0.1a*
- Autor: *Sebastian Łukaszewski*

# Changelog
## Ostatnie 2 wersje reszta w CHANGELOG.md
__0.0.1a__
- [Development] Ustalenie stałej struktury projektu
- [Development] Stworzenie CHANGELOG.md oraz README.md
- [API] Ustalenie postawowych funkcji oraz inicjalizację

# Dokumentacja
## Wykorzystanie i opis funkcji z api.js

__setStorageData(name, data);__
* name:  nazwa localStorage
* data: JSON obiect z wartościami do zapisania

Przykład:
```javascript
setStorageData("nazwa", { x: 1, y: 2 });
````

__getStorageData(name);__
* name: nazwa localStorage

Przykład:
```javascript
var x = getStorageData("nazwa");
console.log(x);
/* Output Console:
{x: 1, y: 2}
*/
```