// ==UserScript==
// @name         Ramki NI by Yuuki
// @version      1.15.3
// @match        http://*.margonem.pl/
// @match        http://*.margonem.com/
// @grant        none
// @author       Yuuki | API by Priweejt
// ==/UserScript==
var data = {};
const LANG = window._l();
let API = window.API.priw;
const INTERFACE = (typeof API != "undefined" && typeof Engine != "undefined" && typeof margoStorage == "undefined") ? "new" : "old";

function createDisplay() {
    let $display = document.createElement("div");
    $display.classList.add("yuki-display", INTERFACE);
   $display.innerHTML = z;
    return $display;
};

if (localStorage.getItem("yRamki") == null) {
    var dane = {
        "img": "https://i.imgur.com/OM178Yk.gif",
        "legend": "#e100c6",
        "heroic": "#0b4ac1",
        "unique": "#008200",
        "upgraded": "#b9e701"
    }
    localStorage.setItem("yRamki", JSON.stringify(dane));
    const obj = localStorage.getItem("yRamki");
    data = JSON.parse(obj);
} else {
    const obj = localStorage.getItem("yRamki");
    data = JSON.parse(obj);
}

var z = `
         <div>
           Link do ramek: <input type='text' id='img-yuki' class='yuki-input' value='${data.img}'><br>
           Kolor Legend: <input type='color' value='${data.legend}' id='legend-yuki' class='yuki-input-color'><br>
           Kolor Heroików: <input type='color' value='${data.heroic}' id='heroic-yuki' class='yuki-input-color'><br>
           Kolor Unikatów: <input type='color' value='${data.unique}' id='unique-yuki' class='yuki-input-color'><br>
           Kolor Ulepszonych: <input type='color' value='${data.upgraded}' id='upgraded-yuki' class='yuki-input-color'><br><br>
           <button onclick='saveYukiSettings();'>Zapisz</button> <button onclick='resetDefaultYuuki();'>Domyślne ustawienia</button>
         </div>
       `;

API.settings.add({
    txt: LANG == "pl" ? "Nowe ramki by Yuuki" : "New frames by Yuuki",
    id: "yuki-alt-display-type",
    default: true
});
API.emmiter.on("toggle-addon-yuki-alt-display-type", () => {
    message(LANG == "pl" ? "Zmiany będą widoczne po odświeżeniu gry" : "Changes will come into effect once the game has been reloaded");
});
if (API.settings.get("yuki-alt-display-type")) {
    createDisplay().classList.add("alt-display");
    API.addonDisplay.add({
        element: createDisplay(),
        name: "Ramki NI by Yuuki",
        id: "yuki",
        icon: "http://pandora.margonem.pl/obrazki/itemy/nas/naszyj284.gif",
        noForcedWndHeight: true
    });

    $(".highlight").css({
        "backgroundImage": "url(" + data.img + ")"
    });
    var x = `
       .tip-wrapper[data-type=t_item] .item-head .legendary { color: ${data.legend} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .legendary { color: ${data.legend} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .legendary, .tip-wrapper[data-type=t_item] .item-tip-section .legendary{ color: ${data.legend} !IMPORTANT }
       .tip-wrapper[data-item-type=t-leg] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${data.legend},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .tip-wrapper[data-type=t_item] .item-head .heroic { color: ${data.heroic} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .heroic { color: ${data.heroic} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .heroic, .tip-wrapper[data-type=t_item] .item-tip-section .heroic{ color: ${data.heroic} !IMPORTANT }
       .tip-wrapper[data-item-type=t-her] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${data.heroic},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .tip-wrapper[data-type=t_item] .item-head .unique { color: ${data.unique} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .unique { color: ${data.unique} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .unique, .tip-wrapper[data-type=t_item] .item-tip-section .unique{ color: ${data.unique} !IMPORTANT }
       .tip-wrapper[data-item-type=t-uniupg] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${data.unique},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .tip-wrapper[data-type=t_item] .item-head .upgraded { color: ${data.upgraded} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head-section .upgraded { color: ${data.upgraded} !IMPORTANT }
       .tip-wrapper[data-type=t_item] .item-head .upgraded, .tip-wrapper[data-type=t_item] .item-tip-section .upgraded{ color: ${data.upgraded} !IMPORTANT }
       .tip-wrapper[data-item-type=t-upgraded] { box-shadow:0 0 0 0 #2b282a,0 0 0 1px #353131,0 0 0 2px #191311,0 0 0 3px #2b2727,0 0 0 4px #59595a,0 0 0 5px ${data.upgraded},0 0 0 6px #5a585b,0 0 0 7px #2c2625 }

       .yuki-input {
           background: rgba(0, 0, 0, 0.2); border: none; width: 40%; color: white; padding: 5px; resize: none; outline: none;
       }
       .yuki-input-color {
           background: rgba(0, 0, 0, 0.2); border: none; width: 10%; color: white; padding: 5px; resize: none; outline: none;
       }
   `;
    $('head').append('<style>' + x + '</style>');
    var y = `
    function resetDefaultYuuki() {
        const LANG = window._l();
        var dane = {
            "img": "https://i.imgur.com/OM178Yk.gif",
            "legend": "#e100c6",
            "heroic": "#0b4ac1",
            "unique": "#008200",
            "upgraded": "#b9e701"
        }
        localStorage.setItem("yRamki", JSON.stringify(dane));
        message(LANG == "pl" ? "Zmiany będą widoczne po odświeżeniu gry" : "Changes will come into effect once the game has been reloaded");
    }

    function saveYukiSettings() {
        const LANG = window._l();
        var data = {
            "img": $('#img-yuki')[0].value,
            "legend": $('#legend-yuki')[0].value,
            "heroic": $('#heroic-yuki')[0].value,
            "unique": $('#unique-yuki')[0].value,
            "upgraded": $('#upgraded-yuki')[0].value
        }
        localStorage.setItem("yRamki", JSON.stringify(data));
        message(LANG == "pl" ? "Zmiany będą widoczne po odświeżeniu gry" : "Changes will come into effect once the game has been reloaded");

    }
`;
    $('body').append('<script>'+y+'</script>');
}