const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
users = [];
connections = [];

server.listen(process.env.PORT || 3000);
console.log("Server running!");
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function(socket) {
    connections.push(socket);
    io.to(socket.id).emit("data");
    socket.on("nData", function(data) {
        var a = new Array(socket.id);
        var b = new Array(data.nick, data.id);
        users.push(a.concat(b));
        console.log(users);
    });

    socket.on('disconnect', function(data) {
        connections.splice(connections.indexOf(socket), 1);
    });

    socket.on('send message', function(data) {
        io.sockets.emit('new message', {
            msg: data
        });
    });
});