function LootFilter() {
    var __this = this;
    var mainBox = $('<div id="ulootfilter"></div>').css({
            width: 170,
            zIndex: 391,
            background: '#222',
            border: '3px double #A95',
            color: '#FFD700',
            fontFamily: 'Verdana, Arial, sans-serif',
            fontSize: '11px',
            textAlign: 'center',
            position: 'absolute',
            padding: '3px 3px 4px',
            cursor: 'grab',
            height: 30,
            overflow: 'hidden'
        }).draggable({
            opacity: 0.7,
            start: function () {
                g.lock.add('dragging');
                Tip.disable();
                $(this).css({
                    cursor: 'grabbing'
                });
            },
            stop: function () {
                __this.saveSettings();
                g.lock.remove('dragging');
                Tip.enable();
                $(this).css({
                    cursor: 'grab'
                });
            }
        });
    $('<div></div>').attr({
        tip: 'Pokaż więcej opcji'
    }).css({
        backgroundImage: 'url(http://i.imgur.com/JnqHa4K.png)',
        backgroundSize: 15,
        height: '15px',
        width: '15px',
        position: 'absolute',
        top: '2px',
        left: '28px'
    }).toggle(function () {
        mainBox.stop().animate({
            height: 130
        }, 500)
    }, function () {
        mainBox.stop().animate({
            height: 30
        }, 500)
    }).appendTo(mainBox);
    var filterInput = $('<input>').css({
            backgroundColor: '#888',
            border: '1px solid #00ff00',
            padding: '1px',
            width: 50,
            color: '#00ff00'
        }).change(function () {
            __this.saveSettings()
        });
    var autoQuitCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_autoQuitCheckbox').change(function () {
            __this.saveSettings()
        });
    var legendaryCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_legendaryCheckbox').change(function () {
            __this.saveSettings()
        });
    var heroicCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_heroicCheckbox').change(function () {
            __this.saveSettings()
        });
    var uniqueCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_uniqueCheckbox').change(function () {
            __this.saveSettings()
        });
    var mixCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_mixCheckbox').change(function () {
            __this.saveSettings()
        });
    var goldCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_goldCheckbox').change(function () {
            __this.saveSettings()
        });
    var arrowCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_arrowCheckbox').change(function () {
            __this.saveSettings()
        });
    var valueCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_valueCheckbox').change(function () {
            __this.saveSettings()
        });
    var tpCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_tpCheckbox').change(function () {
            __this.saveSettings()
        });
    var runesCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_runesCheckbox').change(function () {
            __this.saveSettings()
        });
    var tytonCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_tytonCheckbox').change(function () {
            __this.saveSettings()
    });
    var serceCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_serceCheckbox').change(function () {
        __this.saveSettings()
    });
    var setAll = $('<button>').text('Zaznacz wszystkie').click((function () {
                var c = true;
                return function () {
                    if (c) {
                        mainBox.children('input').attr('checked', 'checked');
                        c = false;
                        $(this).text('Odznacz wszystkie')
                    } else {
                        mainBox.children('input').removeAttr('checked');
                        c = true;
                        $(this).text('Zaznacz wszystkie')
                    }
                    __this.saveSettings()
                }
            })());
    mainBox.append('<b>LootFilter</b><br>');
    mainBox.append(valueCheckbox).append('<label for="LFME_valueCheckbox">powyżej: </label>').append(filterInput).append('<span> $</span><br>');
    mainBox.append(legendaryCheckbox).append('<label for="LFME_legendaryCheckbox" tip="Łapanie legend" style="color: #fa9a20">*L*</label>');
    mainBox.append(heroicCheckbox).append('<label for="LFME_heroicCheckbox" tip="Łapanie heroików" style="color: #2090fe">*H*</label>');
    mainBox.append(uniqueCheckbox).append('<label for="LFME_uniqueCheckbox" tip="Łapanie unikatów" style="color: #daa520">*U*</label>');
    mainBox.append(mixCheckbox).append('<label for="LFME_mixCheckbox" tip="Łapanie miksturek" style="color: #f0032a">*m*</label>');
    mainBox.append('<br>');
    mainBox.append(arrowCheckbox).append('<label for="LFME_arrowCheckbox" tip="Łapanie strzałek" style="color: brown">*s*</label>');
    mainBox.append(tpCheckbox).append('<label for="LFME_tpCheckbox" tip="Łapanie teleportów" style="color: green">*t*</label>');
    mainBox.append(goldCheckbox).append('<label for="LFME_goldCheckbox" tip="Łapanie złota" style="color: gold">*z*</label>');
    mainBox.append(runesCheckbox).append('<label for="LFME_runesCheckbox" tip="Łapanie run" style="color: maroon">*r*</label>');
    mainBox.append('<br>');
    mainBox.append(autoQuitCheckbox).append('<label for="LFME_autoQuitCheckbox" tip="Auto akceptowanie lootu(lotów)">*Z*</label>');
    mainBox.append(tytonCheckbox).append('<label for="LFME_tytonCheckbox" tip="Łapanie Tytoniu" style="color: lime">*T*</label>');
    mainBox.append(serceCheckbox).append('<label for="LFME_tytonCheckbox" tip="Łapanie Serc" style="color: pink">*S*</label>');
    mainBox.append('<br>');
    mainBox.append(setAll);
    $('body').append(mainBox);
    var tmpLootItem = lootItem;
    var doubleSendBlock = false;
    this.saveSettings = function () {
        localStorage.lootfilterU = 'top:' + mainBox.offset().top + '|left:' + mainBox.offset().left + '|value:' + filterInput.val() + '|autoQuit:' + (autoQuitCheckbox.attr('checked') ? 1 : 0) + '|leg:' + (legendaryCheckbox.attr('checked') ? 1 : 0) + '|her:' + (heroicCheckbox.attr('checked') ? 1 : 0) + '|uni:' + (uniqueCheckbox.attr('checked') ? 1 : 0) + '|mix:' + (mixCheckbox.attr('checked') ? 1 : 0) + '|gold:' + (goldCheckbox.attr('checked') ? 1 : 0) + '|arrow:' + (arrowCheckbox.attr('checked') ? 1 : 0) + '|val:' + (valueCheckbox.attr('checked') ? 1 : 0) + '|tp:' + (tpCheckbox.attr('checked') ? 1 : 0) + '|runes:' + (runesCheckbox.attr('checked') ? 1 : 0)+ '|tyton:' + (tytonCheckbox.attr('checked') ? 1 : 0)+ '|serce:' + (serceCheckbox.attr('checked') ? 1 : 0);
    };
    this.readSettings = function () {
        var settings = getCookie('lootfiltermbu');
        if (!!localStorage.lootfilterU) {
            var settings = localStorage.lootfilterU;
        }
        if (settings) {
            settings = settings.split('|');
            for (i = 0; i < settings.length; i++) {
                var pair = settings[i].split(':');
                switch (pair[0]) {
                case 'top':
                    mainBox.css('top', pair[1] + 'px');
                    break;
                case 'left':
                    mainBox.css('left', pair[1] + 'px');
                    break;
                case 'value':
                    filterInput.val(pair[1]);
                    break;
                case 'autoQuit':
                    if (parseInt(pair[1]) == 1)
                        autoQuitCheckbox.attr('checked', 'checked');
                    break;
                case 'leg':
                    if (parseInt(pair[1]) == 1)
                        legendaryCheckbox.attr('checked', 'checked');
                    break;
                case 'her':
                    if (parseInt(pair[1]) == 1)
                        heroicCheckbox.attr('checked', 'checked');
                    break;
                case 'uni':
                    if (parseInt(pair[1]) == 1)
                        uniqueCheckbox.attr('checked', 'checked');
                    break;
                case 'mix':
                    if (parseInt(pair[1]) == 1)
                        mixCheckbox.attr('checked', 'checked');
                    break;
                case 'gold':
                    if (parseInt(pair[1]) == 1)
                        goldCheckbox.attr('checked', 'checked');
                    break;
                case 'arrow':
                    if (parseInt(pair[1]) == 1)
                        arrowCheckbox.attr('checked', 'checked');
                    break;
                case 'val':
                    if (parseInt(pair[1]) == 1)
                        valueCheckbox.attr('checked', 'checked');
                    break;
                case 'tp':
                    if (parseInt(pair[1]) == 1)
                        tpCheckbox.attr('checked', 'checked');
                    break;
                case 'runes':
                    if (parseInt(pair[1]) == 1)
                        runesCheckbox.attr('checked', 'checked');
                    break;
                case 'tyton':
                    if (parseInt(pair[1]) == 1)
                        tytonCheckbox.attr('checked', 'checked');
                    break;
                case 'serce':
                    if (parseInt(pair[1]) == 1)
                        serceCheckbox.attr('checked', 'checked');
                    break;
                }
            }
        } else {
            legendaryCheckbox.attr('checked', 'checked');
            heroicCheckbox.attr('checked', 'checked');
            uniqueCheckbox.attr('checked', 'checked');
            mixCheckbox.attr('checked', 'checked');
            goldCheckbox.attr('checked', 'checked');
            arrowCheckbox.attr('checked', 'checked');
            valueCheckbox.attr('checked', 'checked');
            tpCheckbox.attr('checked', 'checked');
            runesCheckbox.attr('checked', 'checked');
            tytonCheckbox.attr('checked', 'checked');
            serceCheckbox.attr('checked', 'checked');
        }
    };
    this.readSettings();
    lootItem = function (i) {
        tmpLootItem(i);
        var limit = parseInt(filterInput.val());
        var tytonlimit = 5000;
        var sercelimit = 25000;
        if (((isNaN(limit) || (i.pr >= limit)) && valueCheckbox.attr('checked')) || (i.stat.search(/quest/) >= 0) || (i.stat.search(/legendary/) >= 0 && legendaryCheckbox.attr('checked')) || (i.stat.search(/heroic/) >= 0 && heroicCheckbox.attr('checked')) || (i.stat.search(/unique/) >= 0 && uniqueCheckbox.attr('checked')) || ((i.stat.search(/fullheal/) >= 0 || i.stat.search(/leczy/) >= 0 || i.stat.search(/perheal/) >= 0) && mixCheckbox.attr('checked')) || (i.stat.search(/ammo/) >= 0 && arrowCheckbox.attr('checked')) || (i.stat.search(/gold/) >= 0 && goldCheckbox.attr('checked')) || (i.stat.search(/teleport/) >= 0 && tpCheckbox.attr('checked')) || (i.stat.search(/runes/) >= 0 && runesCheckbox.attr('checked')) || ((i.pr == tytonlimit) && tytonCheckbox.attr('checked')) || ((i.pr == sercelimit) && serceCheckbox.attr('checked'))) {
            setLoots((g.loots.init > 1 ? 2 : 0), i.id.toString());
        } else {
            setLoots(1, i.id.toString());
        }
        if (!doubleSendBlock && autoQuitCheckbox.attr('checked')) {
            doubleSendBlock = true;
            setTimeout(function () {
                sendLoots(1);
                doubleSendBlock = false;
            }, 300);
        }
    }
};
g.loadQueue.push({
    fun: function () {
        var _lfu = new LootFilter();
    },
    data: ''
});
/* UAL */
if (typeof uteksaddonlist == 'undefined') {
    uteksaddonlist = true;
    $.getScript('http://addons2.margonem.pl/get/21/21543verified.js');
}