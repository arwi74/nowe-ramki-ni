function LootFilter() {
    function setLoots(j, h) {
        var f = {
            want: [],
            not: [],
            must: []
        };
        var k = g.loots.want;
        var c = g.loots.not;
        var d = g.loots.must;
        for (var e = 0; e < k.length; e++) {
            var b = k[e];
            if ($("#loot" + b).hasClass("yours")) {
                continue
            }
            if (j != false && b == j) {
                f[h].push(j)
            } else {
                f.want.push(b)
            }
        }
        for (var e = 0; e < c.length; e++) {
            var b = c[e];
            if ($("#loot" + b).hasClass("yours")) {
                continue
            }
            if (j != false && b == j) {
                f[h].push(j)
            } else {
                f.not.push(b)
            }
        }
        for (var e = 0; e < d.length; e++) {
            var b = d[e];
            if ($("#loot" + b).hasClass("yours")) {
                continue
            }
            if (j != false && b == j) {
                f[h].push(j)
            } else {
                f.must.push(b)
            }
        }
        g.loots.want = f.want;
        g.loots.not = f.not;
        g.loots.must = f.must;
    }

    function createLine() {
        let newLine = $('<div class="line">').css({
            display: 'flex',
            lineHeight: '19px',
            maxHeight: '19px'
        });
        for (var i = 0; i < arguments.length; i++) {
            newLine.append(arguments[i]);
        }
        return newLine;
    }

    var __this = this;
    var mainBox = $('<div id="ulootfilter"></div>').css({
        width: 150,
        zIndex: 500,
        background: '#000',
        border: '2px solid #202020',
        color: '#e3e3e3',
        fontFamily: 'Arial, sans-serif',
        fontSize: '11px',
        textAlign: 'left',
        position: 'absolute',
        padding: '3px',
        cursor: 'grab',
        height: 38,
        overflow: 'hidden',
        borderRadius: '4px',
        boxShadow: '0 0 8px rgba(0, 0, 0, 0.8)'
    }).draggable({
        opacity: 0.7,
        start: function() {
            g.lock.add('dragging');
            Tip.disable();
            $(this).css({
                cursor: 'grabbing'
            });
        },
        stop: function() {
            __this.saveSettings();
            g.lock.remove('dragging');
            Tip.enable();
            $(this).css({
                cursor: 'grab'
            });
        }
    });
    $('<div></div>').attr({
        tip: 'Pokakaż więcej opcji'
    }).css({
        backgroundImage: 'url(http://i.imgur.com/JnqHa4K.png)',
        backgroundSize: 15,
        height: '15px',
        width: '15px',
        position: 'absolute',
        top: '3px',
        right: '3px'
    }).toggle(function() {
        mainBox.stop().animate({
            height: 310
        }, 500)
    }, function() {
        mainBox.stop().animate({
            height: 38
        }, 500)
    }).appendTo(mainBox);
    var filterInput = $('<input>').css({
        backgroundColor: '#000',
        border: '1px solid #202020',
        padding: '1px',
        width: 50,
        color: '#e3e3e3',
        margin: 'auto 3px',
        borderRadius: '3px',
        fontSize: '11px'
    }).change(function() {
        __this.saveSettings()
    });
    var autoQuitCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_autoQuitCheckbox').change(function() {
        __this.saveSettings()
    });
    var legendaryCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_legendaryCheckbox').change(function() {
        __this.saveSettings()
    });
    var heroicCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_heroicCheckbox').change(function() {
        __this.saveSettings()
    });
    var uniqueCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_uniqueCheckbox').change(function() {
        __this.saveSettings()
    });
    var mixCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_mixCheckbox').change(function() {
        __this.saveSettings()
    });
    var goldCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_goldCheckbox').change(function() {
        __this.saveSettings()
    });
    var arrowCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_arrowCheckbox').change(function() {
        __this.saveSettings()
    });
    var valueCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_valueCheckbox').change(function() {
        __this.saveSettings()
    });
    var tpCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_tpCheckbox').change(function() {
        __this.saveSettings()
    });
    var runesCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_runesCheckbox').change(function() {
        __this.saveSettings()
    });
    var blessCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_blessCheckbox').change(function() {
        __this.saveSettings()
    });
    var tytonCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_tytonCheckbox').change(function () {
        __this.saveSettings()
    });
    var serceCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_serceCheckbox').change(function () {
        __this.saveSettings()
    });
    var glowaCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_glowaCheckbox').change(function () {
        __this.saveSettings()
    });
    
    var torbaCheckbox = $('<input>').attr('type', 'checkbox').attr('id', 'LFME_torbaCheckbox').change(function () {
        __this.saveSettings()
    });
    mainBox.append(createLine('<span style="font-weight: 700; padding-left: 3px">Loot Filter'));
    mainBox.append(createLine(valueCheckbox, '<label for="LFME_valueCheckbox" tip="Łapanie przedmiotów od określonej wartości">Powyżej: </label>', filterInput));
    mainBox.append($('<separator style="display: flex; height: 3px; border-bottom: 1px dotted #202020; margin-bottom: 3px">'));
    mainBox.append(createLine(legendaryCheckbox, '<label for="LFME_legendaryCheckbox" tip="Łapanie legend" style="color: #fa9a20">Legendarne</label>'));
    mainBox.append(createLine(heroicCheckbox, '<label for="LFME_heroicCheckbox" tip="Łapanie heroików" style="color: #2090fe">Heroiczne</label>'));
    mainBox.append(createLine(uniqueCheckbox, '<label for="LFME_uniqueCheckbox" tip="Łapanie unikatów" style="color: #daa520">Unikatowe</label>'));
    mainBox.append(createLine(mixCheckbox, '<label for="LFME_mixCheckbox" tip="Łapanie miksturek" style="color: #f0032a">Mikstury</label>'));
    mainBox.append(createLine(arrowCheckbox, '<label for="LFME_arrowCheckbox" tip="Łapanie strzałek" style="color: brown">Strzały</label>'));
    mainBox.append(createLine(tpCheckbox, '<label for="LFME_tpCheckbox" tip="Łapanie teleportów" style="color: green">Teleporty</label>'));
    mainBox.append(createLine(goldCheckbox, '<label for="LFME_goldCheckbox" tip="Łapanie złota" style="color: gold">Złoto</label>'));
    mainBox.append(createLine(runesCheckbox, '<label for="LFME_runesCheckbox" tip="Łapanie run" style="color: maroon">Runy</label>'));
    mainBox.append(createLine(blessCheckbox, '<label for="LFME_blessCheckbox" tip="Łapanie błogosławieństw" style="color: purple">Błogosławieństwa</label>'));
    mainBox.append(createLine(tytonCheckbox, '<label for="LFME_tytonCheckbox" tip="Łapanie Tytoniu" style="color: lime">Tytoń</label>'));
    mainBox.append(createLine(serceCheckbox, '<label for="LFME_tytonCheckbox" tip="Łapanie Serc" style="color: pink">Serca</label>'));
    mainBox.append(createLine(glowaCheckbox, '<label for="LFME_glowaCheckbox" tip="Łapanie Głów" style="color: lime">Głowy</label>'));
    mainBox.append(createLine(torbaCheckbox, '<label for="LFME_torbaCheckbox" tip="Łapanie Toreb" style="color: gray">Torby</label>'));
    mainBox.append(createLine(autoQuitCheckbox, '<label for="LFME_autoQuitCheckbox" tip="Automatyczne akceptowanie Łupów">Akceptowanie Łupów</label>'));
    $('body').append(mainBox);
    $('line > input[type="checkbox"]').css({
        margin: 'auto 3px'
    });
    this.saveSettings = function() {
        localStorage.lootfilterU = 'top:' + mainBox.offset().top + '|left:' + mainBox.offset().left + '|value:' + filterInput.val() + '|autoQuit:' + (autoQuitCheckbox.attr('checked') ? 1 : 0) + '|leg:' + (legendaryCheckbox.attr('checked') ? 1 : 0) + '|her:' + (heroicCheckbox.attr('checked') ? 1 : 0) + '|uni:' + (uniqueCheckbox.attr('checked') ? 1 : 0) + '|bless:' + (blessCheckbox.attr('checked') ? 1 : 0) + '|mix:' + (mixCheckbox.attr('checked') ? 1 : 0) + '|gold:' + (goldCheckbox.attr('checked') ? 1 : 0) + '|arrow:' + (arrowCheckbox.attr('checked') ? 1 : 0) + '|val:' + (valueCheckbox.attr('checked') ? 1 : 0) + '|tp:' + (tpCheckbox.attr('checked') ? 1 : 0) + '|runes:' + (runesCheckbox.attr('checked') ? 1 : 0) + '|tyton:' + (tytonCheckbox.attr('checked') ? 1 : 0)+ '|serce:' + (serceCheckbox.attr('checked') ? 1 : 0) + '|glowa:'+(glowaCheckbox.attr('checked') ? 1 : 0)  + '|torba:'+(torbaCheckbox.attr('checked') ? 1 : 0);
    };
    this.readSettings = function() {
        var settings = getCookie('lootfiltermbu');
        if (!!localStorage.lootfilterU) {
            var settings = localStorage.lootfilterU;
        }
        if (settings) {
            settings = settings.split('|');
            for (i = 0; i < settings.length; i++) {
                var pair = settings[i].split(':');
                switch (pair[0]) {
                    case 'top':
                        mainBox.css('top', pair[1] + 'px');
                        break;
                    case 'left':
                        mainBox.css('left', pair[1] + 'px');
                        break;
                    case 'value':
                        filterInput.val(pair[1]);
                        break;
                    case 'autoQuit':
                        if (parseInt(pair[1]) == 1)
                            autoQuitCheckbox.attr('checked', 'checked');
                        break;
                    case 'leg':
                        if (parseInt(pair[1]) == 1)
                            legendaryCheckbox.attr('checked', 'checked');
                        break;
                    case 'her':
                        if (parseInt(pair[1]) == 1)
                            heroicCheckbox.attr('checked', 'checked');
                        break;
                    case 'uni':
                        if (parseInt(pair[1]) == 1)
                            uniqueCheckbox.attr('checked', 'checked');
                        break;
                    case 'mix':
                        if (parseInt(pair[1]) == 1)
                            mixCheckbox.attr('checked', 'checked');
                        break;
                    case 'gold':
                        if (parseInt(pair[1]) == 1)
                            goldCheckbox.attr('checked', 'checked');
                        break;
                    case 'arrow':
                        if (parseInt(pair[1]) == 1)
                            arrowCheckbox.attr('checked', 'checked');
                        break;
                    case 'val':
                        if (parseInt(pair[1]) == 1)
                            valueCheckbox.attr('checked', 'checked');
                        break;
                    case 'tp':
                        if (parseInt(pair[1]) == 1)
                            tpCheckbox.attr('checked', 'checked');
                        break;
                    case 'runes':
                        if (parseInt(pair[1]) == 1)
                            runesCheckbox.attr('checked', 'checked');
                        break;
                    case 'bless':
                        if (parseInt(pair[1]) == 1)
                            blessCheckbox.attr('checked', 'checked');
                        break;
                    case 'tyton':
                        if (parseInt(pair[1]) == 1)
                            tytonCheckbox.attr('checked', 'checked');
                        break;
                    case 'serce':
                        if (parseInt(pair[1]) == 1)
                            serceCheckbox.attr('checked', 'checked');
                        break;
                    case 'glowa':
                        if (parseInt(pair[1]) == 1)
                            glowaCheckbox.attr('checked', 'checked');
                        break;
                    case 'torba':
                        if (parseInt(pair[1]) == 1)
                            torbaCheckbox.attr('checked', 'checked');
                        break;
                }
            }
        } else {
            legendaryCheckbox.attr('checked', 'checked');
            heroicCheckbox.attr('checked', 'checked');
            uniqueCheckbox.attr('checked', 'checked');
            mixCheckbox.attr('checked', 'checked');
            goldCheckbox.attr('checked', 'checked');
            arrowCheckbox.attr('checked', 'checked');
            valueCheckbox.attr('checked', 'checked');
            tpCheckbox.attr('checked', 'checked');
            runesCheckbox.attr('checked', 'checked');
            blessCheckbox.attr('checked', 'checked');
            tytonCheckbox.attr('checked', 'checked');
            serceCheckbox.attr('checked', 'checked');
            glowaCheckbox.attr('checked', 'checked');
            torbaCheckbox.attr('checked', 'checked');
        }
    };
    this.readSettings();
    var tmpLootboxItem = lootboxItem;
    var doubleSendBlock = false;
    lootboxItem = function(i) {
        tmpLootboxItem(i);
        var u = parseItemStat(i.stat);
        var limit = parseInt(filterInput.val());
        var tytonlimit = 5000;
        var glowalimit = 7;
        var sercelimit = 25000;
        var torbalimit = 100000;
        if (((isNaN(limit) || (i.pr >= limit)) && valueCheckbox.attr('checked')) || (i.stat.search(/ttl/) >= 0 && blessCheckbox.attr('checked')) || (i.stat.search(/quest/) >= 0) || (i.stat.search(/legendary/) >= 0 && legendaryCheckbox.attr('checked')) || (i.stat.search(/heroic/) >= 0 && heroicCheckbox.attr('checked')) || (i.stat.search(/unique/) >= 0 && uniqueCheckbox.attr('checked')) || ((i.stat.search(/fullheal/) >= 0 || i.stat.search(/leczy/) >= 0 || i.stat.search(/perheal/) >= 0) && mixCheckbox.attr('checked')) || (i.stat.search(/ammo/) >= 0 && arrowCheckbox.attr('checked')) || (i.stat.search(/gold/) >= 0 && goldCheckbox.attr('checked')) || (i.stat.search(/teleport/) >= 0 && tpCheckbox.attr('checked')) || (i.stat.search(/runes/) >= 0 && runesCheckbox.attr('checked') || (i.pr == tytonlimit) && tytonCheckbox.attr('checked')) || ((i.pr == sercelimit) && serceCheckbox.attr('checked')) || ((i.pr == glowalimit) && glowaCheckbox.attr('checked')) || ((i.pr == torbalimit) && torbaCheckbox.attr('checked'))) {
            if (g.party && !(isset(u.reqp) && u.reqp.indexOf(hero.prof) == -1)) {
                setLoots(i.id, "must");
                setStateOnOneLootItem(i.id, 2);
            }
        } else {
            setLoots(i.id, "not");
            setStateOnOneLootItem(i.id, 0);
        }
        if (!doubleSendBlock) {
            doubleSendBlock = true;
            setTimeout(function() {
                autoQuitCheckbox.attr('checked') ? sendLoots(1, false) : sendLoots(0, false);
                doubleSendBlock = false;
            }, 300);
        }
    }
};
g.loadQueue.push({
    fun: function() {
        var _lfu = new LootFilter();
    },
    data: ''
});